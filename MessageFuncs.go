package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/andersfylling/disgord"
)

var (
	msgCache                  *WatchedMessageCache
	spawnDMScriptCache        *SpawnerDMScriptCache
	timerCache                *WatchedTimerCache
	botUserID                 disgord.Snowflake
	createSpawnerMsgChansChan chan map[disgord.Snowflake]*WatchedChannelCache = make(chan map[disgord.Snowflake]*WatchedChannelCache)
	msgCreateChan             chan *disgord.MessageCreate                     = make(chan *disgord.MessageCreate)
	csetupIntCreateHandler    chan *disgord.InteractionCreate                 = make(chan *disgord.InteractionCreate)
	cteardownIntCreateHandler chan *disgord.InteractionCreate                 = make(chan *disgord.InteractionCreate)
)

var noCtx = context.Background()

func csetupHandler() {
	for {
		intCreate, active := <-csetupIntCreateHandler
		if !active {
			log.Debug("event interactions channel no longer active")
		}
		if intCreate.ApplicationID != appID {
			continue
		}
		if intCreate.Type != disgord.InteractionApplicationCommand {
			continue
		}
		if intCreate.Data.Type != disgord.ApplicationCommandChatInput || intCreate.Data.Name != "csetup" {
			continue
		}
		if msgCache.ChannelHasSpawner(intCreate.ChannelID) {
			continue
		}

		data := intCreate.Data
		numOptions := len(data.Options)
		if numOptions < 3 && numOptions > 0 {
			resp := &disgord.CreateInteractionResponse{
				Type: disgord.InteractionCallbackChannelMessageWithSource,
				Data: &disgord.CreateInteractionResponseData{
					Embeds: []*disgord.Embed{
						{
							Description: "There must be only 3 channel given or none.",
						},
					},
				},
			}
			err := client.SendInteractionResponse(noCtx, intCreate, resp)
			logError(err, "csetup: create session post spawning message")
			continue
		} else if numOptions > 3 {
			resp := &disgord.CreateInteractionResponse{
				Type: disgord.InteractionCallbackChannelMessageWithSource,
				Data: &disgord.CreateInteractionResponseData{
					Embeds: []*disgord.Embed{
						{
							Description: "There must be only 3 channel given or none.",
						},
					},
				},
			}
			err := client.SendInteractionResponse(noCtx, intCreate, resp)
			logError(err, "csetup: create session post spawning message")
			continue
		}

		embed := &disgord.Embed{
			Title:       "Session Post Creator",
			Description: "Which game are you playing?",
			Color:       15105570, //orange
		}
		styles := []disgord.ButtonStyle{
			disgord.Secondary,
			disgord.Secondary,
			disgord.Secondary,
		}
		labels := []string{
			"MHGU",
			"MHR",
			"Stories 2",
		}
		buttons := NewTextButtons(len(labels), labels, styles)
		resp := &disgord.CreateInteractionResponse{
			Type: disgord.InteractionCallbackChannelMessageWithSource,
			Data: &disgord.CreateInteractionResponseData{
				Embeds:     []*disgord.Embed{embed},
				Components: buttons.Root,
			},
		}
		err := client.SendInteractionResponse(noCtx, intCreate, resp)
		logError(err, "csetup: create session post spawning message")

		wchans := NewWatchedChannelCache()
		if numOptions == 3 {
			chans := make([]disgord.Snowflake, 3)
			err = nil
			for i := 0; i < len(data.Options); i++ {
				switch data.Options[i].Name {
				case "mghu channel":
					{
						chans[0], err = StringToSnowflake(data.Options[0].Value.(string))
					}
				case "mhr channel":
					{
						chans[1], err = StringToSnowflake(data.Options[1].Value.(string))
					}
				case "mhs2 channel":
					{
						chans[2], err = StringToSnowflake(data.Options[2].Value.(string))
					}
				}
				logError(err, "converting channel ID string to snowflake")
			}

			wchans.Add(&WatchedChannel{
				GameName: "MHGU",
				ID:       chans[0],
			})
			wchans.Add(&WatchedChannel{
				GameName: "MHR",
				ID:       chans[1],
			})
			wchans.Add(&WatchedChannel{
				GameName: "Stories 2",
				ID:       chans[2],
			})
		}
		createSpawnerMsgChansChan <- map[disgord.Snowflake]*WatchedChannelCache{intCreate.ChannelID: wchans}
	}
}

func cteardownHandler() {
	for {
		intCreate, active := <-cteardownIntCreateHandler
		log.Debug("HELLO WORLD 0")
		if !active {
			log.Debug("event interactions channel no longer active")
		}
		log.Debug("HELLO WORLD 1")
		if intCreate.ApplicationID != appID {
			continue
		}
		log.Debug("HELLO WORLD 2")
		if intCreate.Type != disgord.InteractionApplicationCommand {
			continue
		}
		log.Debug("HELLO WORLD 3")
		if intCreate.Data.Type != disgord.ApplicationCommandChatInput || intCreate.Data.Name != "cteardown" {
			continue
		}
		log.Debug("HELLO WORLD 4")
		if !msgCache.ChannelHasSpawner(intCreate.ChannelID) {
			continue
		}
		log.Debug("HELLO WORLD 5")

		perms, e := client.Guild(intCreate.GuildID).Member(intCreate.Member.UserID).GetPermissions()
		if e != nil {
			logError(e, "cdelete")
			return
		}
		log.Debug("HELLO WORLD 6")

		if (perms & disgord.PermissionAdministrator) != disgord.PermissionAdministrator {
			return
		}
		log.Debug("HELLO WORLD 7")

		wmsg, e := msgCache.GetSpawnerByChannel(intCreate.ChannelID)
		if e != nil {
			logError(e, "cdelete")
			return
		}
		log.Debug("HELLO WORLD 8")
		e = client.Channel(wmsg.ChannelID).Message(wmsg.ID).Delete()
		logError(e, "cdelete")
		e = msgCache.Remove(wmsg.ID)
		logError(e, "cdelete")
		c, err := dbpool.Acquire(noCtx)
		logError(err, "cdelete: conn acquire")
		DeleteMessage(c, wmsg.ID)
		c.Release()
		log.Debug("HELLO WORLD 9")
	}
}

func cdelete(s disgord.Session, data *disgord.MessageCreate) {
	msg := data.Message
	if !(msgCache.ChannelHasSpawner(msg.ChannelID)) {
		return
	}

	perms, e := s.Guild(msg.GuildID).Member(data.Message.Author.ID).GetPermissions()
	if e != nil {
		logError(e, "cdelete")
		return
	}

	if (perms & disgord.PermissionAdministrator) != disgord.PermissionAdministrator {
		return
	}

	wmsg, e := msgCache.GetSpawnerByChannel(msg.ChannelID)
	if e != nil {
		logError(e, "cdelete")
		return
	}
	e = s.Channel(wmsg.ChannelID).Message(wmsg.ID).Delete()
	logError(e, "cdelete")
	e = msgCache.Remove(wmsg.ID)
	logError(e, "cdelete")
	c, err := dbpool.Acquire(noCtx)
	logError(err, "cdelete: conn acquire")
	DeleteMessage(c, wmsg.ID)
	c.Release()
}

func csetupCreateMessageHandler() {
	for {
		spawnerChans, active := <-createSpawnerMsgChansChan
		if !active {
			log.Debug("spawner channels channel no longer active")
			break
		}
		for {
			msgCreate, msgActive := <-msgCreateChan
			if !msgActive {
				log.Debug("message create channel no longer active")
				continue
			}

			chans, ok := spawnerChans[msgCreate.Message.ChannelID]
			if !ok {
				continue
			}

			numEmbeds := len(msgCreate.Message.Embeds)
			numParentComponents := len(msgCreate.Message.Components)
			numChildComponents := len(msgCreate.Message.Components[0].Components)
			if numEmbeds != 1 || numParentComponents != 1 || numChildComponents != 3 {
				continue
			}

			embedTitle := msgCreate.Message.Embeds[0].Title
			if embedTitle != "Session Post Creator" {
				continue
			}

			msg := msgCreate.Message
			wmsg := NewWatchedMessage(
				msg.Timestamp,
				msg.ID,
				botUserID,
				msg.ChannelID,
				msg.GuildID,
				Spawner,
				chans,
			)
			msgCache.Add(wmsg)
			c, err := dbpool.Acquire(noCtx)
			logError(err, "csetup: conn acquire")
			AddMessage(c, wmsg)
			c.Release()
			break
		}
	}
}

func messageCreateHandler(s disgord.Session, data *disgord.MessageCreate) {
	// words := strings.Split(data.Message.Content, " ")

	// if words[0] == "$csetup" {
	// 	numOptArgs := len(words) - 1
	// 	if numOptArgs == 0 {
	// 		csetup(s, data.Message, []string{})
	// 	} else if numOptArgs == 3 {
	// 		csetup(s, data.Message, words[1:4])
	// 	}
	// 	return
	// } else if words[0] == "$cdelete" {
	// 	cdelete(s, data)
	// 	return
	// }

	c, err := s.Channel(data.Message.ChannelID).Get()
	if err != nil {
		logError(err, "messageCreateHandler")
	}

	if c.Type == disgord.ChannelTypeDM && spawnDMScriptCache.Has(data.Message.Author.ID) {
		m := data.Message
		dmScript, err := spawnDMScriptCache.Get(data.Message.Author.ID)
		logError(err, "messageCreateHandler")
		if err != nil {
			return
		}
		msgContent := strings.TrimSpace(m.Content)
		if msgContent == "stop" {
			err = spawnDMScriptCache.Remove(m.Author.ID)
			if err != nil {
				logError(err, "messageCreateHandler")
				return
			}
			s.Channel(data.Message.ChannelID).CreateMessage(&disgord.CreateMessage{Content: "Canceling session post creation."})
			con, err := dbpool.Acquire(noCtx)
			logError(err, "delete dm script: conn acquire")
			DeleteDMScript(con, dmScript.UserID)
			con.Release()
		} else if !(dmScript.GaveSessionID) {
			dmScript.GaveSessionID = true
			dmScript.SessionID = msgContent
			s.Channel(data.Message.ChannelID).CreateMessage(&disgord.CreateMessage{Content: "Enter your session description"})
			con, err := dbpool.Acquire(noCtx)
			logError(err, "update dm script session id: conn acquire")
			UpdateDMScript(con, dmScript)
			con.Release()
		} else if !(dmScript.GaveDescription) {
			dmScript.GaveDescription = true
			dmScript.Description = msgContent

			styles := []disgord.ButtonStyle{
				disgord.Secondary,
				disgord.Secondary,
				disgord.Secondary,
			}
			labels := []string{
				"Required",
				"Optional",
				"No voice chat",
			}
			buttons := NewTextButtons(len(labels), labels, styles)
			msgParams := &disgord.CreateMessage{
				Content:    "Voice chat preference? Must currently be in a voice channel if using voice",
				Components: buttons.Root,
			}
			vmsg, err := s.Channel(data.Message.ChannelID).CreateMessage(msgParams)
			logError(err, "messageCreateHandler")
			wmsg := NewWatchedMessage(
				vmsg.Timestamp,
				vmsg.ID,
				dmScript.UserID,
				vmsg.ID,
				vmsg.GuildID,
				VoiceMenu,
				NewWatchedChannelCache(),
			)
			msgCache.Add(wmsg)
			c, err := dbpool.Acquire(noCtx)
			logError(err, "csetup: conn acquire")
			AddMessage(c, wmsg)
			UpdateDMScript(c, dmScript)
			c.Release()
		}
	}
}

func interactionCreateHandler(s disgord.Session, data *disgord.InteractionCreate) {
	wmsg, err := msgCache.Get(data.Message.ID)

	if err != nil {
		logError(err, "interactionCreateHandler")
		return
	}

	if wmsg == nil {
		logError(fmt.Errorf("nil watched message with ID %q", data.Message.ID), "interactionCreateHandler")
		return
	}

	switch wmsg.Type {
	case Spawner:
		//assumption 1: this is always in a guild
		resp := &disgord.CreateInteractionResponse{Type: disgord.InteractionCallbackChannelMessageWithSource}
		s.SendInteractionResponse(noCtx, data, resp)

		if msgCache.UserHasSession(data.Member.User.ID) {
			return
		}

		var gameName string
		switch data.Data.CustomID {
		case "0":
			gameName = "MHGU"
		case "1":
			gameName = "MHR"
		case "2":
			gameName = "Stories 2"
		}
		dmScript := NewSpawnerDMScript(
			data.Member.User.ID,
			data.ChannelID,
			data.GuildID,
			data.Message.ID,
		)
		dmScript.GameName = gameName
		c, e := s.User(dmScript.UserID).CreateDM()
		if e != nil {
			logError(e, "interactionCreateHandler")
			return
		}
		s.Channel(c.ID).CreateMessage(&disgord.CreateMessage{Content: "Enter your session ID."})
		spawnDMScriptCache.Add(dmScript)
		time.AfterFunc(maxDMScriptLifetime, func() {
			if spawnDMScriptCache.Has(dmScript.UserID) {
				e := spawnDMScriptCache.Remove(dmScript.UserID)
				logError(e, "interactionCreateHandler")
				if e == nil {
					s.Channel(c.ID).CreateMessage(&disgord.CreateMessage{Content: "Canceling session post creation."})
				}
				con, err := dbpool.Acquire(noCtx)
				logError(err, "delete dm script: conn acquire")
				DeleteDMScript(con, dmScript.UserID)
				con.Release()
			}
		})
		conn, err := dbpool.Acquire(noCtx)
		logError(err, "create spawner dm script: conn acquire")
		AddDMScript(conn, dmScript)
		conn.Release()
	case Session:
		//assumption 1: this is always in a guild
		if !(msgCache.UserHasSpecificSession(data.Member.User.ID, data.Message.ID)) {
			return
		}

		msg := data.Message
		switch data.Data.CustomID {
		case "0":
			e := s.Channel(msg.ChannelID).Message(msg.ID).Delete()
			logError(e, "interactionCreateHandler")
			e = msgCache.Remove(msg.ID)
			logError(e, "interactionCreateHandler")
			if timerCache.Has(msg.ID) {
				wt, e := timerCache.Get(msg.ID)
				logError(e, "interactionCreateHandler")
				e = timerCache.Remove(msg.ID)
				logError(e, "interactionCreateHandler")
				wt.Timer.Stop()
			}
			c, err := dbpool.Acquire(noCtx)
			logError(err, "delete session: conn acquire")
			DeleteMessage(c, msg.ID)
			c.Release()
		case "1":
			if !(timerCache.Has(msg.ID)) {
				return
			}

			wt, e := timerCache.Get(msg.ID)
			logError(e, "interactionCreateHandler")
			e = timerCache.Remove(msg.ID)
			logError(e, "interactionCreateHandler")
			wt.Timer.Stop()

			t := time.AfterFunc(maxPostLifetime, func() {
				if msgCache.UserHasSession(wmsg.AuthorID) {
					e := s.Channel(msg.ChannelID).Message(msg.ID).Delete()
					logError(e, "interactionCreateHandler")
					e = msgCache.Remove(msg.ID)
					logError(e, "interactionCreateHandler")
					c, err := dbpool.Acquire(noCtx)
					logError(err, "voice menu destroy: conn acquire")
					DeleteMessage(c, msg.ID)
					c.Release()
				}
				if timerCache.Has(msg.ID) {
					e := timerCache.Remove(msg.ID)
					logError(e, "interactionCreateHandler")
				}
			})
			timerCache.Add(NewWatchedTimer(msg.ID, t))

			wmsg.TimesRefreshed += 1
			emb := msg.Embeds[0]
			for i := 0; i < len(emb.Fields); i++ {
				if emb.Fields[i].Name == "Expires" {
					emb.Fields[i].Value = buildExpireFieldText(disgord.Time{Time: time.Now()})
				}
			}
			resp := &disgord.CreateInteractionResponse{
				Type: disgord.InteractionCallbackDeferredUpdateMessage,
				Data: &disgord.CreateInteractionResponseData{
					Embeds: []*disgord.Embed{emb},
				},
			}
			err := s.SendInteractionResponse(noCtx, data, resp)
			logError(err, "interactionCreateHandler")
			c, err := dbpool.Acquire(noCtx)
			logError(err, "session status change: conn acquire")
			UpdateMessage(c, msg.ID, wmsg.TimesRefreshed, wmsg.IsFullSession)
			c.Release()
		case "2":
			emb := msg.Embeds[0]
			if wmsg.IsFullSession {
				wmsg.IsFullSession = false
				sessionID := strings.ReplaceAll(emb.Title, "FULL SESSION: ", "")
				emb.Title = sessionID
				emb.Color = 0 //default
			} else {
				wmsg.IsFullSession = true
				sessionID := emb.Title
				emb.Title = fmt.Sprintf("FULL SESSION: %s", sessionID)
				emb.Color = 15158332 //red
			}
			resp := &disgord.CreateInteractionResponse{
				Type: disgord.InteractionCallbackDeferredUpdateMessage,
				Data: &disgord.CreateInteractionResponseData{
					Embeds: []*disgord.Embed{emb},
				},
			}
			err := s.SendInteractionResponse(noCtx, data, resp)
			logError(err, "interactionResponse:2")
			c, err := dbpool.Acquire(noCtx)
			logError(err, "session status change: conn acquire")
			UpdateMessage(c, msg.ID, wmsg.TimesRefreshed, wmsg.IsFullSession)
			c.Release()
		}
	case VoiceMenu:
		//assumption 1: this will always be in a DM
		dmScript, err := spawnDMScriptCache.Get(wmsg.AuthorID)
		if err != nil {
			logError(err, "messageCreateHandler")
			return
		}
		err = spawnDMScriptCache.Remove(dmScript.UserID)
		if err != nil {
			logError(err, "messageCreateHandler")
			return
		}
		memb, err := s.Guild(dmScript.GuildID).Member(dmScript.UserID).Get()
		if err != nil {
			logError(err, "messageCreateHandler")
			return
		}

		var imgFile io.Reader
		var imgFilepath string
		var imgFilename string
		switch dmScript.GameName {
		case "MHGU":
			imgFilename = "mh-gen-ulti.png"
			imgFilepath = fmt.Sprintf("/imgs/%s", imgFilename)
			imgFile, err = os.Open(imgFilepath)
			if err != nil {
				logError(err, "open image file")
			}
		case "MHR":
			imgFilename = "mh-rise-sunbreak.png"
			imgFilepath = fmt.Sprintf("/imgs/%s", imgFilename)
			imgFile, err = os.Open(imgFilepath)
			if err != nil {
				logError(err, "open image file")
			}
		case "Stories 2":
			imgFilename = "mh-stories2.png"
			imgFilepath = fmt.Sprintf("/imgs/%s", imgFilename)
			imgFile, err = os.Open(imgFilepath)
			if err != nil {
				logError(err, "open image file")
			}
		}
		discFiles := []disgord.CreateMessageFile{{Reader: imgFile, FileName: imgFilename, SpoilerTag: false}}

		var name string
		if memb.Nick == "" {
			name = data.User.Username
		} else {
			name = memb.Nick
		}
		// CHANGE ON THE RELEASE OF DISGORD THAT HAS A BUGFIX FOR VOICESTATE CACHE UPDATES
		// build the session voice channel name
		guild, err := s.Guild(dmScript.GuildID).Get()
		if err != nil {
			logError(err, "messageCreateHandler")
			return
		}
		var vsChannelName string
		hasVoiceState := false
		vcs := voiceStates.States(guild.ID)
		for i := 0; i < len(vcs); i++ {
			fmt.Printf("user %s channel %s", vcs[i].UserID, vcs[i].ChannelID)
			if vcs[i].UserID == dmScript.UserID {
				vsChannelName = fmt.Sprintf("<#%s>", vcs[i].ChannelID)
				hasVoiceState = true
				break
			}
		}
		if !hasVoiceState {
			vsChannelName = string(rune('\u200b'))
		}
		var embedFields []*disgord.EmbedField
		switch data.Data.CustomID {
		case "0":
			embedFields = []*disgord.EmbedField{
				{
					Name:  "Voice Chat",
					Value: "Required",
				},
				{
					Name:  "VoiceChannel",
					Value: vsChannelName,
				},
			}
		case "1":
			embedFields = []*disgord.EmbedField{
				{
					Name:  "Voice Chat",
					Value: "Optional",
				},
				{
					Name:  "VoiceChannel",
					Value: vsChannelName,
				},
			}
		case "2":
			embedFields = []*disgord.EmbedField{
				{
					Name:  "Voice Chat",
					Value: "None",
				},
			}
		}
		embedFields = append(
			embedFields,
			&disgord.EmbedField{
				Name:  "Expires",
				Value: buildExpireFieldText(disgord.Time{Time: time.Now()}),
			},
		)
		embedFoot := &disgord.EmbedFooter{
			IconURL: fmt.Sprintf("https://cdn.discordapp.com/avatars/%s/%s.png", memb.User.ID, memb.User.Avatar),
			Text: fmt.Sprintf(
				"%s#%s",
				name,
				memb.User.Discriminator,
			),
		}
		embedImg := &disgord.EmbedImage{
			URL: fmt.Sprintf("attachment://%s", imgFilename),
		}
		embedAuth := &disgord.EmbedAuthor{
			Name: name,
		}
		embed := &disgord.Embed{
			Title:       fmt.Sprintf("Session %s", dmScript.SessionID),
			Description: dmScript.Description,
			Fields:      embedFields,
			Footer:      embedFoot,
			Image:       embedImg,
			Author:      embedAuth,
		}
		emojis := []*disgord.Emoji{
			{Name: "🗑️"},
			{Name: "🔁"},
			{Name: "🚧"},
		}
		styles := []disgord.ButtonStyle{
			disgord.Secondary,
			disgord.Secondary,
			disgord.Secondary,
		}
		labels := []string{
			"Destroy",
			"Refresh",
			"Filled",
		}

		buttons := NewEmojiButtons(len(emojis), emojis, labels, styles)
		msgParams := &disgord.CreateMessage{
			Embed:      embed,
			Components: buttons.Root,
			Files:      discFiles,
		}

		spawnermsg, err := msgCache.Get(dmScript.SpawnerMessageID)
		if err != nil {
			logError(err, "cannot get spawner message")
		}
		var msg *disgord.Message
		var spawnedPost *WatchedMessage
		if spawnermsg.SpawnsInMultipleChannels() {
			channel, err := spawnermsg.GameChannels.GetByName(dmScript.GameName)
			if err != nil {
				logError(err, fmt.Sprintf("messageReactionAddHandler: Cannot get channel for %s", dmScript.GameName))
				return
			}
			msg, err = s.Channel(channel.ID).CreateMessage(msgParams)
			if err != nil {
				logError(err, fmt.Sprintf("messageReactionAddHandler: Cannot post message in guild %q in channel %q", dmScript.GuildID, channel.ID))
				return
			}
			spawnedPost = NewWatchedMessage(
				msg.Timestamp,
				msg.ID,
				dmScript.UserID,
				channel.ID,
				msg.GuildID,
				Session,
				NewWatchedChannelCache(),
			)
		} else {
			msg, err := s.Channel(dmScript.ChannelID).CreateMessage(msgParams)
			if err != nil {
				logError(err, fmt.Sprintf("messageReactionAddHandler: Cannot post message in guild %q in channel %q", dmScript.GuildID, dmScript.ChannelID))
				return
			}
			spawnedPost = NewWatchedMessage(
				msg.Timestamp,
				msg.ID,
				dmScript.UserID,
				msg.ChannelID,
				msg.GuildID,
				Session,
				NewWatchedChannelCache(),
			)
		}
		msgCache.Add(spawnedPost)
		timer := time.AfterFunc(maxPostLifetime, func() {
			if msgCache.UserHasSession(dmScript.UserID) {
				e := s.Channel(spawnedPost.ChannelID).Message(spawnedPost.ID).Delete()
				logError(e, "interactionCreateHandler")
				e = msgCache.Remove(spawnedPost.ID)
				logError(e, "interactionCreateHandler")
				c, err := dbpool.Acquire(noCtx)
				logError(err, "session destroy: conn acquire")
				DeleteMessage(c, spawnedPost.ID)
				c.Release()
			}
			if timerCache.Has(spawnedPost.ID) {
				e := timerCache.Remove(spawnedPost.ID)
				logError(e, "interactionCreateHandler")
			}
		})
		timerCache.Add(NewWatchedTimer(spawnedPost.ID, timer))
		s.Channel(data.Message.ChannelID).CreateMessage(&disgord.CreateMessage{Content: "Session post created."})
		msgCache.Remove(wmsg.ID)
		c, err := dbpool.Acquire(noCtx)
		logError(err, "session create: conn acquire")
		AddMessage(c, spawnedPost)
		DeleteMessage(c, wmsg.ID)
		DeleteDMScript(c, dmScript.UserID)
		c.Release()
	default:
		logError(fmt.Errorf("%q is an unknown message type", wmsg.Type), "interactionCreateHandler")
	}
}
