package main

import (
	"testing"
	"github.com/andersfylling/disgord"
)

func TestParseEmoji(t *testing.T) {
	cases := []struct {
		in string
		exp interface{}
	}{
		{"898609149225615402", disgord.Snowflake(898609149225615402)},
		{"<:npc:898609149225615402>", disgord.Snowflake(898609149225615402)},
	}

	for _, c := range cases {
		out, err := ParseEmoji(c.in)
		if err != nil {
			t.Errorf("ERROR: ParseEmoji(%q) %q", c.in, err)
		}
		if c.exp != out {
			t.Errorf("ParseEmoji(%q) == %q, expected %q", c.in, out, c.exp)
		}
	}
}
