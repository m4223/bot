package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/andersfylling/disgord"
	"github.com/andersfylling/disgord/std"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/sirupsen/logrus"
)

var (
	log                 *logrus.Logger
	maxPostLifetime     time.Duration
	maxDMScriptLifetime time.Duration
	voiceStates         *voiceStateTracker
	dbpool              *pgxpool.Pool
	client              disgord.Session
	appID               disgord.Snowflake
	commands            []*disgord.CreateApplicationCommand = []*disgord.CreateApplicationCommand{
		{
			Name:        "csetup",
			Description: "Creates a message in the channel that can be used to create session posts.",
			Type:        disgord.ApplicationCommandChatInput,
			Options: []*disgord.ApplicationCommandOption{
				{
					Type:        disgord.OptionTypeChannel,
					Name:        "mghu-chan",
					Description: "monster hunter generations ultimate channel",
				},
				{
					Type:        disgord.OptionTypeChannel,
					Name:        "mhr-chan",
					Description: "monster hunter rise channel",
				},
				{
					Type:        disgord.OptionTypeChannel,
					Name:        "mhs2-chan",
					Description: "monster hunter stories 2 channel",
				},
			},
		},
		{
			Name:        "cteardown",
			Description: "Removes the messaged in the channel that is used to create session posts.",
			Type:        disgord.ApplicationCommandChatInput,
		},
	}
)

func logError(err error, trace string) {
	if err != nil {
		log.WithFields(logrus.Fields{
			"trace": trace,
		}).Error(err)
	}
}

func main() {
	const (
		cmdPrefix = "$"
	)

	var (
		configFilepath string
	)

	flag.StringVar(&configFilepath, "config_filepath", "", "Filepath to the json configuration file.")
	flag.Parse()

	absConfigFilepath, err := filepath.Abs(configFilepath)
	if err != nil {
		panic(err)
	}
	config := NewConfig(absConfigFilepath)
	appID = disgord.Snowflake(config.AppID)
	log = &logrus.Logger{
		Out:       os.Stderr,
		Formatter: new(logrus.TextFormatter),
		Hooks:     make(logrus.LevelHooks),
		Level:     logrus.Level(config.LogLevel),
	}
	botUserID = disgord.Snowflake(config.BotUserID)
	maxPostLifetime, err = time.ParseDuration(config.MaxPostLifetime)
	if err != nil {
		panic(err)
	}
	maxDMScriptLifetime, err = time.ParseDuration(config.MaxDMScriptLifetime)
	if err != nil {
		panic(err)
	}

	msgCache = NewWatchedMessageCache()
	spawnDMScriptCache = NewSpawnerDMScriptCache()
	timerCache = NewWatchedTimerCache()

	dbpool, err = pgxpool.Connect(
		context.Background(),
		fmt.Sprintf(
			"postgres://%s:%s@%s:%s/%s",
			config.DBUsername,
			config.DBUserPassword,
			config.DBIP,
			config.DBPort,
			config.DBName,
		),
	)
	if err != nil {
		panic(err)
	}
	defer dbpool.Close()

	client = disgord.New(disgord.Config{
		ProjectName: config.BotName,
		BotToken:    config.DiscordToken,
		Logger:      log,
		RejectEvents: []string{
			disgord.EvtTypingStart,
			disgord.EvtPresenceUpdate,
			disgord.EvtGuildMemberAdd,
			disgord.EvtGuildMemberUpdate,
			disgord.EvtGuildMemberRemove,
		},
		DMIntents: disgord.IntentDirectMessages,
	})
	defer client.Gateway().StayConnectedUntilInterrupted()

	logFilter, _ := std.NewLogFilter(client)
	filter, _ := std.NewMsgFilter(context.Background(), client)
	filter.SetPrefix(cmdPrefix)

	//event handlers
	// client.Gateway().WithMiddleware(
	// 	filter.NotByBot,
	// 	logFilter.LogMsg,
	// ).MessageCreate(messageCreateHandler)
	client.Gateway().WithMiddleware(
		logFilter.LogMsg,
	).MessageCreateChan(msgCreateChan)
	// client.Gateway().InteractionCreate(interactionCreateHandler)
	client.Gateway().InteractionCreateChan(
		csetupIntCreateHandler,
		cteardownIntCreateHandler,
	)
	client.Gateway().BotReady(func() {
		log.Info(fmt.Sprintf("%s ready", config.BotName))

		for i := 0; i < len(commands); i++ {
			if err := client.ApplicationCommand(appID).Global().Create(commands[i]); err != nil {
				logError(err, "app-cmd-create")
			}
		}
	})

	gw := client.Gateway().WithContext(context.TODO())

	voiceStates = NewVoiceStateTracker()
	voiceStates.Register(gw)

	go func() {
		c, err := dbpool.Acquire(noCtx)
		logError(err, "init-startup-from-db")
		Startup(client, c)
	}()

	go csetupHandler()
	go cteardownHandler()
	go csetupCreateMessageHandler()
}
