package main

import (
	"fmt"

	"github.com/andersfylling/disgord"
)

type WatchableMessageComponent interface {
	Length() int
	Get(string) (*disgord.MessageComponent, error)
}

/*
	Structure:
		[
			actionRow: [
				button1,
				button2,
				button3,
				buttonN
			]
		]
*/
type EmojiButtons struct {
	Root []*disgord.MessageComponent
}

//array elements are added to buttons in the order they are sent.
func NewEmojiButtons(
	totalButtons int,
	emojis []*disgord.Emoji,
	labels []string,
	styles []disgord.ButtonStyle,
) *EmojiButtons {
	//these should just flat out fail
	if totalButtons != len(emojis) {
		panic(fmt.Sprintf("Number of buttons %q must be equal to the number of emojis %q.", totalButtons, len(emojis)))
	}

	if totalButtons != len(labels) {
		panic(fmt.Sprintf("Number of buttons %q must be equal to the number of labels %q.", totalButtons, len(labels)))
	}

	if totalButtons != len(styles) {
		panic(fmt.Sprintf("Number of buttons %q must be equal to the number of styles %q.", totalButtons, len(styles)))
	}

	root := make([]*disgord.MessageComponent, 1)
	root[0] = &disgord.MessageComponent{}
	root[0].Type = disgord.MessageComponentActionRow

	buttons := make([]*disgord.MessageComponent, totalButtons)
	for i := 0; i < totalButtons; i++ {
		buttons[i] = &disgord.MessageComponent{
			Type:     disgord.MessageComponentButton,
			Style:    styles[i],
			Emoji:    emojis[i],
			Label:    labels[i],
			CustomID: fmt.Sprintf("%v", i),
		}
	}
	root[0].Components = buttons
	return &EmojiButtons{Root: root}
}

func (e *EmojiButtons) Length() int {
	return len(e.Root[0].Components)
}

func (e *EmojiButtons) Get(customID string) (*disgord.MessageComponent, error) {
	for i := 0; i < e.Length(); i++ {
		if e.Root[0].Components[i].CustomID == customID {
			return e.Root[0].Components[i], nil
		}
	}

	return nil, fmt.Errorf("unable to find component %s", customID)
}

func (e *EmojiButtons) GetStyles() []disgord.ButtonStyle {
	total := e.Length()
	a := make([]disgord.ButtonStyle, total)
	for i := 0; i < total; i++ {
		a[i] = e.Root[0].Components[i].Style
	}
	return a
}

func (e *EmojiButtons) GetLabels() []string {
	total := e.Length()
	a := make([]string, total)
	for i := 0; i < total; i++ {
		a[i] = e.Root[0].Components[i].Label
	}
	return a
}

func (e *EmojiButtons) GetEmojis() []*disgord.Emoji {
	total := e.Length()
	a := make([]*disgord.Emoji, total)
	for i := 0; i < total; i++ {
		a[i] = e.Root[0].Components[i].Emoji
	}
	return a
}

/*
	Structure:
		[
			actionRow: [
				button1,
				button2,
				button3,
				buttonN
			]
		]
*/
type TextButtons struct {
	Root []*disgord.MessageComponent
}

//array elements are added to buttons in the order they are sent.
func NewTextButtons(
	totalButtons int,
	labels []string,
	styles []disgord.ButtonStyle,
) *TextButtons {
	//these should just flat out fail
	if totalButtons != len(labels) {
		panic(fmt.Sprintf("Number of buttons %q must be equal to the number of labels %q.", totalButtons, len(labels)))
	}

	if totalButtons != len(styles) {
		panic(fmt.Sprintf("Number of buttons %q must be equal to the number of styles %q.", totalButtons, len(styles)))
	}

	root := make([]*disgord.MessageComponent, 1)
	root[0] = &disgord.MessageComponent{}
	root[0].Type = disgord.MessageComponentActionRow

	buttons := make([]*disgord.MessageComponent, totalButtons)
	for i := 0; i < totalButtons; i++ {
		buttons[i] = &disgord.MessageComponent{
			Type:     disgord.MessageComponentButton,
			Style:    styles[i],
			Label:    labels[i],
			CustomID: fmt.Sprintf("%v", i),
		}
	}
	root[0].Components = buttons
	return &TextButtons{Root: root}
}

func (e *TextButtons) Length() int {
	return len(e.Root[0].Components)
}

func (e *TextButtons) Get(customID string) (*disgord.MessageComponent, error) {
	for i := 0; i < e.Length(); i++ {
		if e.Root[0].Components[i].CustomID == customID {
			return e.Root[0].Components[i], nil
		}
	}

	return nil, fmt.Errorf("unable to find component %s", customID)
}

func (e *TextButtons) GetStyles() []disgord.ButtonStyle {
	total := e.Length()
	a := make([]disgord.ButtonStyle, total)
	for i := 0; i < total; i++ {
		a[i] = e.Root[0].Components[i].Style
	}
	return a
}

func (e *TextButtons) GetLabels() []string {
	total := e.Length()
	a := make([]string, total)
	for i := 0; i < total; i++ {
		a[i] = e.Root[0].Components[i].Label
	}
	return a
}
