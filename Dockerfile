FROM golang:1.17.2-alpine3.14

RUN mkdir /imgs

WORKDIR /app

COPY ./go.mod ./
COPY ./go.sum ./
RUN go mod download

COPY ./*.go ./
COPY ./imgs/*.png /imgs/

RUN go build -o /app/bot

ENTRYPOINT [ "/app/bot" ]
