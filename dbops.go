package main

import (
	"time"

	"github.com/andersfylling/disgord"
	"github.com/jackc/pgx/v4/pgxpool"
)

func AddDMScript(c *pgxpool.Conn, script *SpawnerDMScript) {
	_, err := c.Exec(
		noCtx,
		"insert into sessions.spawner_dm_scripts(created_id, user_id, channel_id, guild_id, spawner_message_id, session_id, game_name, description, gave_session_id, gave_desc, gave_voice_chat) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)",
		script.Timestamp.Time,
		script.UserID.String(),
		script.ChannelID.String(),
		script.GuildID.String(),
		script.SpawnerMessageID.String(),
		script.SessionID,
		script.GameName,
		script.Description,
		script.GaveSessionID,
		script.GaveDescription,
		script.GaveVoiceChat,
	)
	logError(err, "add-dm-script")
}

func UpdateDMScript(
	c *pgxpool.Conn,
	script *SpawnerDMScript,
) {
	_, err := c.Exec(
		noCtx,
		"update sessions.spawner_dm_scripts set session_id=$1, game_name=$2, description=$3, gave_session_id=$4, gave_desc=$5, gave_voice_chat=$6 where user_id=$7",
		script.SessionID,
		script.GameName,
		script.Description,
		script.GaveSessionID,
		script.GaveDescription,
		script.GaveVoiceChat,
		script.UserID.String(),
	)
	logError(err, "update-dm-script")
}

func DeleteDMScript(c *pgxpool.Conn, userID disgord.Snowflake) {
	_, err := c.Exec(
		noCtx,
		"delete from sessions.spawner_dm_scripts where user_id=$1",
		userID.String(),
	)
	if err != nil {
		logError(err, "delete-dm-script")
	}
}

func AddMessage(c *pgxpool.Conn, msg *WatchedMessage) {
	_, err := c.Exec(
		noCtx,
		"insert into sessions.messages(created_on, id, author_id, channel_id, guild_id, times_refreshed, type, is_full_session) values($1, $2, $3, $4, $5, $6, $7, $8)",
		msg.Timestamp.Time,
		msg.ID.String(),
		msg.AuthorID.String(),
		msg.ChannelID.String(),
		msg.GuildID.String(),
		msg.TimesRefreshed,
		int64(msg.Type),
		msg.IsFullSession,
	)
	if err != nil {
		logError(err, "add-msg")
	}

	switch msg.Type {
	case Spawner:
		for i := 0; i < msg.GameChannels.Length(); i++ {
			_, err = c.Exec(
				noCtx,
				"insert into sessions.channel_caches(message_id, channel_id, channel_name) values($1, $2, $3)",
				msg.ID.String(),
				//not thread safe. make a Next() command later
				msg.GameChannels.channels[i].ID.String(),
				msg.GameChannels.channels[i].GameName,
			)
			if err != nil {
				logError(err, "spawner-chan")
			}
		}
	}
}

func UpdateMessage(c *pgxpool.Conn, msgID disgord.Snowflake, timesRefreshed int64, isFullSession bool) {
	_, err := c.Exec(
		noCtx,
		"update sessions.messages set times_refreshed=$1, is_full_session=$2 where id=$3",
		timesRefreshed,
		isFullSession,
		msgID.String(),
	)
	if err != nil {
		logError(err, "update-msg")
	}
}

func DeleteMessage(c *pgxpool.Conn, msgID disgord.Snowflake) {
	_, err := c.Exec(
		noCtx,
		"delete from sessions.messages where id=$1",
		msgID.String(),
	)
	if err != nil {
		logError(err, "delete-msg")
	}
}

func Startup(s disgord.Session, c *pgxpool.Conn) {
	defer c.Release()

	msgRows, err := c.Query(noCtx, "select * from sessions.messages")
	if err != nil {
		logError(err, "db-startup-msg")
		return
	}

	// collect message metadata
	msgs := []*WatchedMessage{}
	for msgRows.Next() {
		var timestamp time.Time
		var id string
		var uid string
		var cid string
		var gid string
		var timesRefreshed int64
		var t int
		var isFullSession bool
		err = msgRows.Scan(&timestamp, &id, &uid, &cid, &gid, &timesRefreshed, &t, &isFullSession)
		if err != nil {
			logError(err, "db-startup-read-msg")
			return
		}

		ids, err := StringToSnowflake(id)
		logError(err, "db-startup-parse-msg-id-snow-1")
		uids, err := StringToSnowflake(uid)
		logError(err, "db-startup-parse-author-id-snow")
		cids, err := StringToSnowflake(cid)
		logError(err, "db-startup-parse-channel-id-snow-1")
		gids, err := StringToSnowflake(gid)
		logError(err, "db-startup-parse-guild-id-snow")
		msgs = append(msgs, NewWatchedMessage(
			disgord.Time{Time: timestamp},
			ids,
			uids,
			cids,
			gids,
			MessageType(t),
			NewWatchedChannelCache(),
		))
	}
	msgRows.Close()

	chanCacheRows, err := c.Query(noCtx, "select * from sessions.channel_caches")
	if err != nil {
		logError(err, "db-startup-chan")
		return
	}

	// collect channel metadata for spawner messages
	chanCaches := make(map[disgord.Snowflake]*WatchedChannelCache)
	for chanCacheRows.Next() {
		var mid string
		var cid string
		var gameName string
		err = chanCacheRows.Scan(&mid, &cid, &gameName)
		if err != nil {
			logError(err, "db-startup-read-chan-caches")
			return
		}
		cids, err := StringToSnowflake(cid)
		logError(err, "db-startup-parse-channel-id-snow-2")
		mids, err := StringToSnowflake(mid)
		logError(err, "db-startup-parse-msg-id-snow-2")
		wChan := NewWatchedChannel(gameName, cids)
		if val, ok := chanCaches[mids]; ok {
			err = val.Add(wChan)
			logError(err, "db-startup-build-chan-caches-2")
		} else {
			t := NewWatchedChannelCache()
			err = t.Add(wChan)
			logError(err, "db-startup-build-chan-caches-1")
			chanCaches[mids] = t
		}
	}
	chanCacheRows.Close()

	// add channel caches to spawner posts & put messages into message cache
	for i := 0; i < len(msgs); i++ {
		authorID := msgs[i].AuthorID
		chanID := msgs[i].ChannelID
		msgID := msgs[i].ID
		switch msgs[i].Type {
		case Spawner:
			msgs[i].GameChannels = chanCaches[msgID]
		case Session:
			// update each session post's text to show the new timer to the user
			discmsg, err := s.Channel(chanID).Message(msgID).Get()
			logError(err, "messageCreateHandler")
			msgs[i].TimesRefreshed += 1
			emb := discmsg.Embeds[0]
			for i := 0; i < len(emb.Fields); i++ {
				if emb.Fields[i].Name == "Expires" {
					emb.Fields[i].Value = buildExpireFieldText(disgord.Time{Time: time.Now()})
				}
			}
			_, e := s.Channel(chanID).Message(msgID).Update(&disgord.UpdateMessage{
				Embeds: &[]*disgord.Embed{emb},
			})
			logError(e, "interactionCreateHandler")
			UpdateMessage(c, msgID, msgs[i].TimesRefreshed, msgs[i].IsFullSession)

			// reset timers when starting back up
			timer := time.AfterFunc(maxPostLifetime, func() {
				if msgCache.UserHasSession(authorID) {
					e := s.Channel(chanID).Message(msgID).Delete()
					logError(e, "interactionCreateHandler")
					e = msgCache.Remove(msgID)
					logError(e, "interactionCreateHandler")
					innerc, err := dbpool.Acquire(noCtx)
					logError(err, "session destroy: conn acquire")
					DeleteMessage(innerc, msgID)
				}
				if timerCache.Has(msgID) {
					e := timerCache.Remove(msgID)
					logError(e, "interactionCreateHandler")
				}
			})
			timerCache.Add(NewWatchedTimer(msgID, timer))
		case VoiceMenu:
			// reset timers when starting back up
			timer := time.AfterFunc(maxPostLifetime, func() {
				if msgCache.UserHasSession(authorID) {
					e := s.Channel(chanID).Message(msgID).Delete()
					logError(e, "interactionCreateHandler")
					e = msgCache.Remove(msgID)
					logError(e, "interactionCreateHandler")
					innerc, err := dbpool.Acquire(noCtx)
					logError(err, "session destroy: conn acquire")
					DeleteMessage(innerc, msgID)
				}
				if timerCache.Has(msgID) {
					e := timerCache.Remove(msgID)
					logError(e, "interactionCreateHandler")
				}
			})
			timerCache.Add(NewWatchedTimer(msgID, timer))
		}
		err := msgCache.Add(msgs[i])
		logError(err, "db-startup-build-msg-cache")
	}

	dmScriptRows, err := c.Query(noCtx, "select * from sessions.spawner_dm_scripts")
	if err != nil {
		logError(err, "db-startup-dm-scripts")
		return
	}

	// collect channel metadata for spawner messages
	dmScripts := []*SpawnerDMScript{}
	for dmScriptRows.Next() {
		var timestamp time.Time
		var uid string
		var cid string
		var gid string
		var smid string
		var sid string
		var gameName string
		var desc string
		var gaveSessionID bool
		var gaveDescription bool
		var gaveVoiceChat bool
		err = dmScriptRows.Scan(&timestamp, &uid, &cid, &gid, &smid, &sid, &gameName, &desc, &gaveSessionID, &gaveDescription, &gaveVoiceChat)
		if err != nil {
			logError(err, "db-startup-read-dm-script")
			return
		}

		uids, err := StringToSnowflake(uid)
		logError(err, "db-startup-parse-user-id-snow")
		cids, err := StringToSnowflake(cid)
		logError(err, "db-startup-parse-channel-id-snow")
		gids, err := StringToSnowflake(gid)
		logError(err, "db-startup-parse-guild-id-snow")
		smids, err := StringToSnowflake(smid)
		logError(err, "db-startup-parse-spawner-message-id-snow")
		script := NewSpawnerDMScript(
			uids,
			cids,
			gids,
			smids,
		)
		script.Timestamp = disgord.Time{Time: timestamp}
		script.SessionID = sid
		script.GameName = gameName
		script.Description = desc
		script.GaveSessionID = gaveSessionID
		script.GaveDescription = gaveDescription
		script.GaveVoiceChat = gaveVoiceChat
		dmScripts = append(dmScripts, script)
	}
	dmScriptRows.Close()

	for i := 0; i < len(dmScripts); i++ {
		uid := dmScripts[i].UserID
		chanID := dmScripts[i].ChannelID
		// reset spawner dm script lifetimes
		time.AfterFunc(maxDMScriptLifetime, func() {
			if spawnDMScriptCache.Has(uid) {
				e := spawnDMScriptCache.Remove(uid)
				logError(e, "interactionCreateHandler")
				if e == nil {
					s.Channel(chanID).CreateMessage(&disgord.CreateMessage{Content: "Canceling session post creation."})
				}
				con, err := dbpool.Acquire(noCtx)
				logError(err, "delete dm script: conn acquire")
				DeleteDMScript(con, uid)
			}
		})
		spawnDMScriptCache.Add(dmScripts[i])
	}
}
