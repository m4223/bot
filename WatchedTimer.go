package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/andersfylling/disgord"
)

type WatchedTimer struct {
	MessageID disgord.Snowflake
	Timer     *time.Timer
}

func NewWatchedTimer(id disgord.Snowflake, timer *time.Timer) *WatchedTimer {
	return &WatchedTimer{
		MessageID: id,
		Timer:     timer,
	}
}

type WatchedTimerCache struct {
	sync.Mutex
	timers []*WatchedTimer
}

func NewWatchedTimerCache() *WatchedTimerCache {
	return &WatchedTimerCache{timers: []*WatchedTimer{}}
}

func (w *WatchedTimerCache) Length() int {
	w.Lock()
	defer w.Unlock()
	return len(w.timers)
}

func (w *WatchedTimerCache) get(mid disgord.Snowflake) (*WatchedTimer, error) {
	total := len(w.timers)
	for i := 0; i < total; i++ {
		if w.timers[i] == nil {
			continue
		} else if w.timers[i].MessageID == mid {
			return w.timers[i], nil
		}
	}
	return nil, fmt.Errorf("cannot find timer for message %q", mid)
}

func (w *WatchedTimerCache) has(mid disgord.Snowflake) bool {
	_, err := w.get(mid)
	return err == nil
}

func (w *WatchedTimerCache) Get(mid disgord.Snowflake) (*WatchedTimer, error) {
	w.Lock()
	defer w.Unlock()
	return w.get(mid)
}

func (w *WatchedTimerCache) Has(mid disgord.Snowflake) bool {
	w.Lock()
	defer w.Unlock()
	return w.has(mid)
}

func (w *WatchedTimerCache) Add(t *WatchedTimer) error {
	w.Lock()
	defer w.Unlock()
	if w.has(t.MessageID) {
		return fmt.Errorf("message %q already has a timer", t.MessageID)
	}

	total := len(w.timers)
	new := make([]*WatchedTimer, total+1)
	copy(new, w.timers)
	new[total] = t
	w.timers = new
	return nil
}

func (w *WatchedTimerCache) Remove(mid disgord.Snowflake) error {
	w.Lock()
	defer w.Unlock()
	if !(w.has(mid)) {
		return fmt.Errorf("unable to delete timer for message %q", mid)
	}

	//count number of valid messages
	total := len(w.timers)
	numValidEles := 0
	for i := 0; i < total; i++ {
		if w.timers[i].MessageID == mid || w.timers[i] == nil {
			continue
		}
		numValidEles++
	}

	//create new slice with invalid messages removed
	new := make([]*WatchedTimer, numValidEles)
	counter := 0
	for i := 0; i < total; i++ {
		if w.timers[i].MessageID == mid || w.timers[i] == nil {
			continue
		}
		new[counter] = w.timers[i]
		counter++
	}
	w.timers = new
	return nil
}
