package main

import (
	"os"
	"encoding/json"
)

type Config struct {
	BotName string
	AppID uint64
	BotUserID uint64
	DiscordToken string
	DBUsername string
	DBUserPassword string
	DBIP string
	DBPort string
	DBName string
	LogLevel uint32
	MaxPostLifetime string
	MaxDMScriptLifetime string
}

func NewConfig(configFilepath string) *Config {
	configFile, err := os.Open(configFilepath)
	if err != nil {
		panic(err)
	}
	defer configFile.Close()

	rawConfig := struct {
		BotName string
		AppID uint64
		BotUserID uint64
		DiscordToken string
		DBUsername string
		DBUserPassword string
		DBIP string
		DBPort string
		DBName string
		LogLevel string
		MaxPostLifetime string
		MaxDMScriptLifetime string
	}{}
	err = json.NewDecoder(configFile).Decode(&rawConfig)
	if err != nil {
		panic(err)
	}

	var lvl uint32
	switch rawConfig.LogLevel {
		case "Panic":
			lvl = 0
		case "Fatal":
			lvl = 1
		case "Error":
			lvl = 2
		case "Warn":
			lvl = 3
		case "Info":
			lvl = 4
		case "Debug":
			lvl = 5
		case "Trace":
			lvl = 6
		default:
			lvl = 4
	}
	return &Config{
		BotName: rawConfig.BotName,
		AppID: rawConfig.AppID,
		BotUserID: rawConfig.BotUserID,
		DiscordToken: rawConfig.DiscordToken,
		DBUsername: rawConfig.DBUsername,
		DBUserPassword: rawConfig.DBUserPassword,
		DBIP: rawConfig.DBIP,
		DBPort: rawConfig.DBPort,
		DBName: rawConfig.DBName,
		LogLevel: lvl,
		MaxPostLifetime: rawConfig.MaxPostLifetime,
		MaxDMScriptLifetime: rawConfig.MaxDMScriptLifetime,
	}
}

