package main

import (
	"fmt"
	"sync"

	"github.com/andersfylling/disgord"
)

type MessageType int

const (
	Spawner MessageType = iota
	Session
	VoiceMenu
)

type WatchedMessage struct {
	Timestamp      disgord.Time
	ID             disgord.Snowflake
	AuthorID       disgord.Snowflake
	ChannelID      disgord.Snowflake
	GuildID        disgord.Snowflake
	TimesRefreshed int64
	Type           MessageType
	IsFullSession  bool
	GameChannels   *WatchedChannelCache
}

func NewWatchedMessage(
	timestamp disgord.Time,
	id disgord.Snowflake,
	authorID disgord.Snowflake,
	channelID disgord.Snowflake,
	guildID disgord.Snowflake,
	mtype MessageType,
	gameChannels *WatchedChannelCache,
) *WatchedMessage {
	return &WatchedMessage{
		Timestamp:      timestamp,
		ID:             id,
		AuthorID:       authorID,
		ChannelID:      channelID,
		GuildID:        guildID,
		Type:           mtype,
		TimesRefreshed: 0,
		IsFullSession:  false,
		GameChannels:   gameChannels,
	}
}

func (w *WatchedMessage) SpawnsInMultipleChannels() bool {
	return w.GameChannels.Length() > 0
}

type WatchedMessageCache struct {
	sync.Mutex
	messages []*WatchedMessage
}

func NewWatchedMessageCache() *WatchedMessageCache {
	return &WatchedMessageCache{messages: []*WatchedMessage{}}
}

func (w *WatchedMessageCache) Length() int {
	w.Lock()
	defer w.Unlock()
	return len(w.messages)
}

func (w *WatchedMessageCache) get(id disgord.Snowflake) (*WatchedMessage, error) {
	total := len(w.messages)
	for i := 0; i < total; i++ {
		if w.messages[i] == nil {
			continue
		} else if w.messages[i].ID == id {
			return w.messages[i], nil
		}
	}
	return nil, fmt.Errorf("cannot find message %q", id)
}

func (w *WatchedMessageCache) getSpawnerByChannel(id disgord.Snowflake) (*WatchedMessage, error) {
	total := len(w.messages)
	for i := 0; i < total; i++ {
		if w.messages[i] == nil {
			continue
		} else if w.messages[i].ChannelID == id && w.messages[i].Type == Spawner {
			return w.messages[i], nil
		}
	}
	return nil, fmt.Errorf("cannot find message %q", id)
}

func (w *WatchedMessageCache) has(id disgord.Snowflake) bool {
	_, err := w.get(id)
	return err == nil
}

func (w *WatchedMessageCache) channelHasSpawner(id disgord.Snowflake) bool {
	total := len(w.messages)
	for i := 0; i < total; i++ {
		if w.messages[i] == nil {
			continue
		} else if w.messages[i].ChannelID == id && w.messages[i].Type == Spawner {
			return true
		}
	}
	return false
}

func (w *WatchedMessageCache) userHasSession(id disgord.Snowflake) bool {
	total := len(w.messages)
	for i := 0; i < total; i++ {
		if w.messages[i] == nil {
			continue
		} else if w.messages[i].AuthorID == id && w.messages[i].Type == Session {
			return true
		}
	}
	return false
}

func (w *WatchedMessageCache) userHasSpecificSession(uid, mid disgord.Snowflake) bool {
	total := len(w.messages)
	for i := 0; i < total; i++ {
		if w.messages[i] == nil {
			continue
		} else if w.messages[i].ID == mid && w.messages[i].AuthorID == uid && w.messages[i].Type == Session {
			return true
		}
	}
	return false
}

func (w *WatchedMessageCache) Get(id disgord.Snowflake) (*WatchedMessage, error) {
	w.Lock()
	defer w.Unlock()
	return w.get(id)
}

func (w *WatchedMessageCache) GetSpawnerByChannel(id disgord.Snowflake) (*WatchedMessage, error) {
	w.Lock()
	defer w.Unlock()
	return w.getSpawnerByChannel(id)
}

func (w *WatchedMessageCache) Has(id disgord.Snowflake) bool {
	w.Lock()
	defer w.Unlock()
	return w.has(id)
}

func (w *WatchedMessageCache) ChannelHasSpawner(id disgord.Snowflake) bool {
	w.Lock()
	defer w.Unlock()
	return w.channelHasSpawner(id)
}

func (w *WatchedMessageCache) UserHasSession(id disgord.Snowflake) bool {
	w.Lock()
	defer w.Unlock()
	return w.userHasSession(id)
}

func (w *WatchedMessageCache) UserHasSpecificSession(uid, mid disgord.Snowflake) bool {
	w.Lock()
	defer w.Unlock()
	return w.userHasSpecificSession(uid, mid)
}

func (w *WatchedMessageCache) Add(m *WatchedMessage) error {
	w.Lock()
	defer w.Unlock()
	if w.has(m.ID) {
		return fmt.Errorf("message %q already added to channel %q in guild %q", m.ID, m.ChannelID, m.GuildID)
	}

	total := len(w.messages)
	new := make([]*WatchedMessage, total+1)
	copy(new, w.messages)
	new[total] = m
	w.messages = new
	return nil
}

func (w *WatchedMessageCache) Remove(id disgord.Snowflake) error {
	w.Lock()
	defer w.Unlock()
	if !(w.has(id)) {
		return fmt.Errorf("unable to delete message %q", id)
	}

	//count number of valid messages
	total := len(w.messages)
	numValidEles := 0
	for i := 0; i < total; i++ {
		if w.messages[i].ID == id || w.messages[i] == nil {
			continue
		}
		numValidEles++
	}

	//create new slice with invalid messages removed
	new := make([]*WatchedMessage, numValidEles)
	counter := 0
	for i := 0; i < total; i++ {
		if w.messages[i].ID == id || w.messages[i] == nil {
			continue
		}
		new[counter] = w.messages[i]
		counter++
	}
	w.messages = new
	return nil
}
