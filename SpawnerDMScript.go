package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/andersfylling/disgord"
)

type SpawnerDMScript struct {
	Timestamp        disgord.Time
	UserID           disgord.Snowflake
	ChannelID        disgord.Snowflake
	GuildID          disgord.Snowflake
	SpawnerMessageID disgord.Snowflake
	SessionID        string
	GameName         string
	Description      string
	GaveSessionID    bool
	GaveDescription  bool
	GaveVoiceChat    bool
}

func NewSpawnerDMScript(uid, cid, gid, sid disgord.Snowflake) *SpawnerDMScript {
	return &SpawnerDMScript{
		Timestamp:        disgord.Time{Time: time.Now()},
		UserID:           uid,
		ChannelID:        cid,
		GuildID:          gid,
		SpawnerMessageID: sid,
		SessionID:        "",
		GameName:         "",
		Description:      "",
		GaveSessionID:    false,
		GaveDescription:  false,
		GaveVoiceChat:    false,
	}
}

type SpawnerDMScriptCache struct {
	sync.Mutex
	scripts []*SpawnerDMScript
}

func NewSpawnerDMScriptCache() *SpawnerDMScriptCache {
	return &SpawnerDMScriptCache{scripts: []*SpawnerDMScript{}}
}

func (d *SpawnerDMScriptCache) length() int {
	return len(d.scripts)
}

func (d *SpawnerDMScriptCache) get(id disgord.Snowflake) (*SpawnerDMScript, error) {
	for i := 0; i < len(d.scripts); i++ {
		if d.scripts[i] == nil {
			continue
		} else if d.scripts[i].UserID == id {
			return d.scripts[i], nil
		}
	}
	return nil, fmt.Errorf("user %q does not have a SpawnerDMScript", id)
}

func (d *SpawnerDMScriptCache) has(id disgord.Snowflake) bool {
	_, err := d.get(id)
	return err == nil
}

func (d *SpawnerDMScriptCache) Length() int {
	d.Lock()
	defer d.Unlock()
	return d.length()
}

func (d *SpawnerDMScriptCache) Get(id disgord.Snowflake) (*SpawnerDMScript, error) {
	d.Lock()
	defer d.Unlock()
	return d.get(id)
}

func (d *SpawnerDMScriptCache) Has(id disgord.Snowflake) bool {
	d.Lock()
	defer d.Unlock()
	return d.has(id)
}

func (d *SpawnerDMScriptCache) Add(s *SpawnerDMScript) error {
	d.Lock()
	defer d.Unlock()
	if d.has(s.UserID) {
		return fmt.Errorf("user %q already has a SpawnerDMScript", s.UserID)
	}

	new := make([]*SpawnerDMScript, d.length()+1)
	copy(new, d.scripts)
	new[d.length()] = s
	d.scripts = new
	return nil
}

func (d *SpawnerDMScriptCache) Remove(uid disgord.Snowflake) error {
	d.Lock()
	defer d.Unlock()
	if !(d.has(uid)) {
		return fmt.Errorf("user %q does not have a SpawnerDMScript", uid)
	}

	//count number of valid messages
	total := len(d.scripts)
	numValidEles := 0
	for i := 0; i < total; i++ {
		if d.scripts[i].UserID == uid || d.scripts[i] == nil {
			continue
		}
		numValidEles++
	}

	//create new slice with invalid messages removed
	new := make([]*SpawnerDMScript, numValidEles)
	counter := 0
	for i := 0; i < total; i++ {
		if d.scripts[i].UserID == uid || d.scripts[i] == nil {
			continue
		}
		new[counter] = d.scripts[i]
		counter++
	}
	d.scripts = new
	return nil
}
