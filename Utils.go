package main

import (
	"errors"
	"regexp"
	"strconv"
	"fmt"
	
	"github.com/andersfylling/disgord"
)

func ParseChannelID(channelString string) (disgord.Snowflake, error) {
	rexp := regexp.MustCompile(`^<#([0-9]*)>$`)
	if rexp.Match([]byte(channelString)) {
		id, err := strconv.ParseUint(rexp.FindStringSubmatch(channelString)[1], 10, 64)
		if err != nil {
			return disgord.Snowflake(0), errors.New("ParseChannelID: unable to parse input")
		}
		return disgord.Snowflake(id), nil
	} else {
		return disgord.Snowflake(0), errors.New("ParseChannelID: unable to parse input")
	}
}

func ParseEmoji(emojiString string) (disgord.Snowflake, error) {
	emojiID, err := strconv.ParseUint(emojiString, 10, 64)
	if err != nil {
		rexp := regexp.MustCompile(`^<:[a-zA-Z0-9]*:([0-9]*)>$`)
		if rexp.Match([]byte(emojiString)) {
			return ParseEmoji(rexp.FindStringSubmatch(emojiString)[1])
		} else {
			return disgord.Snowflake(0), errors.New("ParseEmoji: unable to parse input string")
		}
	}

	return disgord.Snowflake(emojiID), nil
}

func StringToSnowflake(s string) (disgord.Snowflake, error) {
	out, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		return disgord.Snowflake(0), err
	}
	return disgord.Snowflake(out), err
}

func buildExpireFieldText(
	baseTime disgord.Time,
) string {
	expdt := baseTime.Add(maxPostLifetime)
	return fmt.Sprintf(
		"<t:%d:R> <t:%d:F>",
		expdt.Unix(),
		expdt.Unix(),
	)
}
