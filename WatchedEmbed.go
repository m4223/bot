package main

import (
	"github.com/andersfylling/disgord"
)

type WatchedEmbed struct {
	disgord.Embed
}

func NewWatchedEmbed() *WatchedEmbed {
	return &WatchedEmbed{disgord.Embed{}}
}

func FromEmbed(e *disgord.Embed) *WatchedEmbed {
	return &WatchedEmbed{disgord.Embed{
		Title: e.Title,
		Type: e.Type,
		Description: e.Description,
		URL: e.URL,
		Timestamp: e.Timestamp,
		Color: e.Color,
		Footer: e.Footer,
		Image: e.Image,
		Thumbnail: e.Thumbnail,
		Video: e.Video,
		Provider: e.Provider,
		Author: e.Author,
		Fields: e.Fields,
	}}
}

func ToEmbed(we *WatchedEmbed) *disgord.Embed {
	return &disgord.Embed{
		Title: we.Title,
		Type: we.Type,
		Description: we.Description,
		URL: we.URL,
		Timestamp: we.Timestamp,
		Color: we.Color,
		Footer: we.Footer,
		Image: we.Image,
		Thumbnail: we.Thumbnail,
		Video: we.Video,
		Provider: we.Provider,
		Author: we.Author,
		Fields: we.Fields,
	}
}
