package main

import (
	"fmt"
	"sync"

	"github.com/andersfylling/disgord"
)

type WatchedChannel struct {
	GameName string
	ID       disgord.Snowflake
}

func NewWatchedChannel(gameName string, cid disgord.Snowflake) *WatchedChannel {
	return &WatchedChannel{
		GameName: gameName,
		ID:       cid,
	}
}

type WatchedChannelCache struct {
	sync.Mutex
	channels []*WatchedChannel
}

func NewWatchedChannelCache() *WatchedChannelCache {
	return &WatchedChannelCache{channels: []*WatchedChannel{}}
}

func (w *WatchedChannelCache) Length() int {
	w.Lock()
	defer w.Unlock()
	return len(w.channels)
}

func (w *WatchedChannelCache) get(id disgord.Snowflake) (*WatchedChannel, error) {
	total := len(w.channels)
	for i := 0; i < total; i++ {
		if w.channels[i] == nil {
			continue
		} else if w.channels[i].ID == id {
			return w.channels[i], nil
		}
	}
	return nil, fmt.Errorf("cannot find channel %q", id)
}

func (w *WatchedChannelCache) getByName(gameName string) (*WatchedChannel, error) {
	total := len(w.channels)
	for i := 0; i < total; i++ {
		if w.channels[i] == nil {
			continue
		} else if w.channels[i].GameName == gameName {
			return w.channels[i], nil
		}
	}
	return nil, fmt.Errorf("cannot find channel with game name %s", gameName)
}

func (w *WatchedChannelCache) has(id disgord.Snowflake) bool {
	_, err := w.get(id)
	return err == nil
}

func (w *WatchedChannelCache) Get(id disgord.Snowflake) (*WatchedChannel, error) {
	w.Lock()
	defer w.Unlock()
	return w.get(id)
}

func (w *WatchedChannelCache) GetByName(gameName string) (*WatchedChannel, error) {
	w.Lock()
	defer w.Unlock()
	return w.getByName(gameName)
}

func (w *WatchedChannelCache) Has(id disgord.Snowflake) bool {
	w.Lock()
	defer w.Unlock()
	return w.has(id)
}

func (w *WatchedChannelCache) Add(m *WatchedChannel) error {
	w.Lock()
	defer w.Unlock()
	if w.has(m.ID) {
		return fmt.Errorf("channel %q already added", m.ID)
	}

	total := len(w.channels)
	new := make([]*WatchedChannel, total+1)
	copy(new, w.channels)
	new[total] = m
	w.channels = new
	return nil
}

func (w *WatchedChannelCache) Remove(id disgord.Snowflake) error {
	w.Lock()
	defer w.Unlock()
	if !(w.has(id)) {
		return fmt.Errorf("unable to delete channel %q", id)
	}

	//count number of valid channels
	total := len(w.channels)
	numValidEles := 0
	for i := 0; i < total; i++ {
		if w.channels[i].ID == id || w.channels[i] == nil {
			continue
		}
		numValidEles++
	}

	//create new slice with invalid channels removed
	new := make([]*WatchedChannel, numValidEles)
	counter := 0
	for i := 0; i < total; i++ {
		if w.channels[i].ID == id || w.channels[i] == nil {
			continue
		}
		new[counter] = w.channels[i]
		counter++
	}
	w.channels = new
	return nil
}
